package main

import (
	"github.com/valyala/fastjson"
	"runtime"
	"strings"
	"sync"
)

func main() {
}

func joinNumbers(logs []byte) string {
	var res string
	parts := divide(logs, runtime.GOMAXPROCS(0))
	wg := &sync.WaitGroup{}

	for n, part := range parts {
		wg.Add(1)

		go func(p []byte, n int) {
			lastI := 0
			var result strings.Builder
			defer wg.Done()

			for i := 0; i < len(p); i++ {
				if p[i] != '\n' {
					continue
				}
				x := jsonJSON(p[lastI:i])

				if x != "" {
					result.WriteString(x)
				}
				lastI = i + 1
			}
			res := result.String()
			if len(res) >= 2 {
				res = res[2:]
			}
		}(part, n)
	}
	wg.Wait()
	return res
}

func jsonJSON(l []byte) string {
	message := fastjson.GetString(l, "message")
	return joinMessage(message)
}

func joinMessage(message string) string {
	var result strings.Builder

	lastI := -1
	for i, c := range message {
		if isNumber(c) {
			continue
		}

		if i-lastI > 1 {
			result.WriteString(", ")
			result.WriteString(message[lastI+1 : i])
		}
		lastI = i
	}

	if len(message)-lastI > 1 {
		result.WriteString(", ")
		result.WriteString(message[lastI+1:])
	}

	return result.String()
}

func isNumber(c rune) bool {
	return '0' <= c && c <= '9'
}

func divide(logs []byte, parts int) [][]byte {
	pos := 0
	step := len(logs) / parts
	result := make([][]byte, 0)

	for pos+step < len(logs) {
		start := pos
		finish := pos + step
		for logs[finish] != '\n' {
			finish++
		}
		finish++
		result = append(result, logs[start:finish])
		pos = finish
	}

	if pos != len(logs) {
		result = append(result, logs[pos:])
	}

	return result
}
