module practice-perf

go 1.13

require (
	github.com/valyala/fastjson v1.5.3
	github.com/vitkovskii/insane-json v0.1.1
)
