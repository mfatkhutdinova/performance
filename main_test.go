package main

import (
	"io/ioutil"
	"os"
	"testing"
)

func BenchmarkCount(b *testing.B) {
	file, err := os.Open("logs")
	if err != nil {
		panic(err.Error())
	}

	logs, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err.Error())
	}

	b.ReportAllocs()
	b.SetBytes(int64(len(logs)))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		joinNumbers(logs)
	}
	b.StopTimer()
}
